# Tech Test

This test submission was built with React using `create-react-app` as recommended. 

I am not a day to day user of React but I felt that this it was better to code in something which you as reviewers will be familiar with as opposed to me being within my comfort zone. 

In addition the JS is written in ES6, more for the benefit of the reviewer than me. I belive ES6 makes for much more readbale code. I am hoping that this display my ability to adapt to new coding styles, frameworks / libraries.

## Clone the reposiory
You can clone the repos in to a folder of your choice
`git clone https://bitbucket.org/chrisrymer/sb/src/master <foldername>`

## Directory structure
The repository holds the docker config in the root and the app. You can start up the api directly from the root of the folder.
1. Open Terminal
2. `cd <target folder>`
3. `docker-compose up`

## Starting the app
The follwing steps presume that docker is already configured

1. Open a terminal and `cd` to `./app` in the main folder
2. Run the `yarn` command to install the dependencies
3. With the docker instance running enter `yarn start` to launch the dev env

Once started you will be launched in to the browser on `http://localhost:3000` and be able to preview the app.
