import React from 'react';
import PropTypes from 'prop-types';

import ListLiveEvents from './ListLiveEvents';

const LiveEvents = (props) => {
  return (
    <div>
      <ListLiveEvents 
        primaryMarket={props.primaryMarket}
        oddsValue={ props.oddsValue }
        events={ props.events } 
        getDate={ props.getDate } />
    </div>
  )
}

LiveEvents.propTypes = {
  primaryMarket: PropTypes.bool.isRequired,
  oddsValue: PropTypes.string.isRequired,
  getDate: PropTypes.func.isRequired,
  events: PropTypes.arrayOf(PropTypes.shape({
    startTime: PropTypes.string.isRequired, 
    name: PropTypes.string.isRequired,
    eventId: PropTypes.number.isRequired,
    linkedEventTypeName: PropTypes.string,
  })),
}

export default LiveEvents;