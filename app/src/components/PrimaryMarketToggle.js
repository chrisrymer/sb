import React from 'react';

const styles = {
  display: 'inline-block',
}
const PrimaryMarketToggle = (props) => {  
  return (
    <div className='toggle toggle-markets' style={styles}>
      <strong>Primary Markets </strong>
      <label htmlFor="primaryTrue">Show</label> <input id="primaryTrue" type="radio" name="primary-market" value="true"  />
      <label htmlFor="primaryFalse">Hide</label> <input id="primaryFalse" type="radio" name="primary-market" value="false" defaultChecked />
    </div>
  )
}

export default PrimaryMarketToggle;