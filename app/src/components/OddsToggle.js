import React from 'react';

const styles = {
  wrapper: {
    display: 'inline-block',
    marginLeft: '1em',
  },
  inputs: {
    display: 'inline-block',
  }
}

const OddsToggle = (props) => {
  const setDefaultChecked = (oddsProp) => {
    let inputs = [];
    if(oddsProp=== 'decimal') {
      inputs.push(<div key='0' style={styles.inputs}>
        <label htmlFor="oddsDecimal">Decimal</label> <input id="oddsDecimal" type="radio" name="odds-toggle" value="decimal"  defaultChecked />
        &nbsp;<label htmlFor="oddsFraction">Fraction</label> <input id="oddsFraction" type="radio" name="odds-toggle" value="fractional" />
      </div>)
    } else {
      inputs.push(<div key='0' style={styles.inputs}>
        <label htmlFor="oddsDecimal">Decimal</label> <input id="oddsDecimal" type="radio" name="odds-toggle" value="decimal" />
        &nbsp;<label htmlFor="oddsFraction">Fraction</label> <input id="oddsFraction" type="radio" name="odds-toggle" value="fractional" defaultChecked />
      </div>)
    }
    
    return (
      <div className='toggle toggle-odds' style={styles.wrapper}>
        <strong>Show odds as </strong> {inputs}
      </div>
    );
  }

  return setDefaultChecked(props.oddsValue);
}

export default OddsToggle;