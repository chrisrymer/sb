import React from 'react';

import './Header.css'
import PropTypes from 'prop-types';

import EventControls from './EventControls';

const Header = (props) => {
  return (
    <header id="eventHeader" className="event-header">
      <nav>
        <a href="/">{props.eventType}</a>
      </nav>
      <EventControls 
        oddsValue={ props.oddsValue }
        toggleOdds={ props.toggleOdds }
        toggleMarkets={props.toggleMarkets}
        primaryMarket={props.primaryMarket} />
    </header>
  )
}

Header.propTypes = {
  primaryMarket: PropTypes.bool.isRequired,
  oddsValue: PropTypes.string.isRequired,
  toggleOdds: PropTypes.func.isRequired,
  toggleMarkets: PropTypes.func.isRequired,
}

export default Header;