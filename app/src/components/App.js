import React, { Component } from 'react';
import axios from 'axios';

import Header from './Header';
import LiveEvents from './LiveEvents';

// 3. Write and fix tests //

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      events: null,
      oddsValue: 'fractional',
      primaryMarket: false,
      eventType: 'football'
    };
  }

  async componentDidMount() {
    try {
      const res = await axios.get(`/${this.state.eventType}/live`);
      const data = res.data
      this.setState(() => ({
        events: data.markets ? this.combineMarketData(data) : data.events,
        isLoaded: true, 
      }));
    } catch(error) {
      this.setState(() => ({
        error: true,
      }));
    }
  }

  toggleOddsHandle = (value) => {
    this.setState((prevState) => ({
      oddsValue: prevState.oddsValue !== value ? value : prevState.oddsValue
    }));
  }

  togglePrimaryMarketHandle = async (value) => {
    let bool = value === 'true';
    await this.getLiveEvents(bool);
    this.setState((prevState) => ({
      primaryMarket: bool,
    }));
  }

  getDate(dateStr) {
    const date = new Date(dateStr);
    let mins = date.getMinutes();
    mins = mins < 10 ? '0' + mins : mins;
    
    return date.getHours() + ':' + mins;
  }

  combineMarketData({ events, outcomes, markets }) {
    return events.map(event => {
      event.market = markets[event.eventId].pop();
      event.outcomes = outcomes[event.market.marketId];
      return event;
    });
  }

  getLiveEvents = async (primaryMarket) => {
    try {
      const hasPrimaryMarket = primaryMarket ? '?primaryMarkets=true' : '';
      const res = await axios.get(`/${this.state.eventType}/live${hasPrimaryMarket}`);

      this.setState(() => ({
        events: res.data.outcomes ? this.combineMarketData(res.data) : res.data.events
      }))
    } catch(error) {
      this.setState(() => ({
        error: true,
      }));
    }
  };

  render() {
    if(this.state.isLoaded) {
      return (
        <div className="App">
          <Header 
            eventType='Football'
            toggleMarkets={ this.togglePrimaryMarketHandle }
            toggleOdds={ this.toggleOddsHandle }
            primaryMarket={ this.state.primaryMarket }
            oddsValue={ this.state.oddsValue }  />
          <LiveEvents 
            getDate={ this.getDate }
            primaryMarket={ this.state.primaryMarket }
            events={ this.state.events }
            oddsValue={ this.state.oddsValue } />
        </div>
      );
    } else if(this.state.error) {
      return <div>Error Connecting to endpoint.</div>
    } else {
      return <div>Loading...</div>
    }
  }
}

export default App;
