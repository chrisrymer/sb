import React from 'react';

import Outcomes from './Outcomes';

const styles = {
  fixtures: {
    row: {
      display: 'block',
      padding: '.5em',
      borderBottom: 'solid 1px #f8f8f8',
      minHeight: '30px',
    },
    start: {
        fontSize: '.8em',
        display: 'inline-block',
        verticalAlign: 'top',
        paddingTop: '2px',
        color: '#009638',
    },
    info: {
        marginLeft: '2em',
        fontSize: '.9em',
        color: '#333',
        fontWeight: 'bold',
        display: 'inline-block',
    },
    linkedEvent: {
      fontSize: '.75em',
      fontWeight: 'normal',
      color: '#cfcfcf',
    }
  }
}

const ListItem = (props) => {
  const { startTime, name, eventId, linkedEventTypeName } = props.event;

  const getDate = (dateStr) => {
    const date = new Date(dateStr);
    let mins = date.getMinutes();
    mins = mins < 10 ? '0' + mins : mins;
    
    return date.getHours() + ':' + mins;
  }

  return (
    <div key={eventId}>
      <a href='markets/eventId/hash' style={ styles.fixtures.row }>
        <div className="start-time" style={ styles.fixtures.start }>{ getDate(startTime) }</div>
        <div className="fixture-info" style={ styles.fixtures.info }>
          <span>{name}</span>
          <br />
          <span className='linked-event' style={ styles.fixtures.linkedEvent }>
            {linkedEventTypeName}
          </span>
        </div>
        {props.event.outcomes ? <Outcomes outcomes={ props.event.outcomes } oddsValue={ props.oddsValue } /> : ''}
      </a>
    </div>
  )
}

export default ListItem;