import React from 'react';

import './EventControls.css'

import OddsToggle from './OddsToggle';
import PrimaryMarketToggle from './PrimaryMarketToggle';

const EventControls = (props) => {
  const eventConroller = (event) => {
    let { name, value } = event.target;

    if(name === 'primary-market') {
      if(value !== props.primaryMarket) {
        props.toggleMarkets(value);
      }
    }

    if(name === 'odds-toggle') {
      props.toggleOdds(value);
    }
  }

  return (
    <form className='event-controls' onChange={(event) => { eventConroller(event) }}>
      <PrimaryMarketToggle  primaryMarket={props.primaryMarket}/>
      { props.primaryMarket ? <OddsToggle oddsValue={props.oddsValue} /> : '' }
    </form>
  )
}

export default EventControls;