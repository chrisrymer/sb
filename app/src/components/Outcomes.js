import React from 'react';
import PropTypes from 'prop-types';

import './Outcomes.css';

const Outcomes = (props) => {
  const {outcomes, oddsValue} = props;
  return (
    <div className='outcomes'>
      {outcomes.map((outcome, i) => 
          <div key={i} className="outcome">
            <span><strong>{outcome.type !== 'draw' ? 'Win' : outcome.name} </strong></span> 
            <span className="odds">{
              oddsValue === 'decimal' ? parseFloat(outcome.price.decimal).toFixed(2)
                : outcome.price.num + '/' + outcome.price.den }</span>
          </div>)}
    </div>
  )
}

Outcomes.propTypes = {
  outcomes: PropTypes.arrayOf(PropTypes.shape({
    type: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    price: PropTypes.shape({
      decimal: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
      ]).isRequired,
      num: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
      ]).isRequired,
      den: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
      ]).isRequired,
    })
  }))
}

export default Outcomes;