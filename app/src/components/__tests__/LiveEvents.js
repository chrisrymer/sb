import '../../test-setup'
import React from 'react';
import { shallow, mount, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import LiveEvents from '../LiveEvents';
LiveEvents.propTypes = {}

describe('<LiveEvents />', () => {
  it('does render the component', () => {
    const dom = shallow(<LiveEvents />);
    expect(dom.length).toBe(1);
  });
});

