import '../../test-setup'
import React from 'react';
import { shallow, mount, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import OddsToggle from '../OddsToggle';

describe('<OddsToggle />', () => {
  it('renders two inputs', () => {
    const inputs = mount(<OddsToggle oddsValue='fractional' />);
    expect(inputs.find('input').length).toBe(2);
  });

  it('accepts the oddsValue prop', () => {
    const inputs = mount(<OddsToggle oddsValue='decimal' />);
    expect(inputs.props()).toEqual({"oddsValue": "decimal"})
  });
})

