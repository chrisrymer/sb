import '../../test-setup'
import React from 'react';
import { shallow, mount, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import ListLiveEvents from '../ListLiveEvents';
ListLiveEvents.propTypes = {}

const props = {
  "events": [
    {
      "eventId": 21249939,
      "name": "Shanghai Shenhua 0 v 0 Shandong Luneng Taishan",
      "displayOrder": -1000,
      "sort": "MTCH",
      "linkedEventId": 21228740,
      "classId": 5,
      "className": "Football",
      "typeId": 10003971,
      "typeName": "Football Live",
      "linkedEventTypeId": 10005942,
      "linkedEventTypeName": "Chinese Super League",
      "startTime": "2017-09-19T11:35:23.000Z",
      "scores": {
        "home": 0,
        "away": 0
      },
      "competitors": [
        {
          "name": "Shanghai Shenhua",
          "position": "home"
        },
        {
          "name": "Shandong Luneng Taishan",
          "position": "away"
        }
      ],
      "status": {
        "active": true,
        "started": true,
        "live": true,
        "resulted": false,
        "finished": false,
        "cashoutable": true,
        "displayable": true,
        "suspended": false,
        "requestabet": false
      },
      "boostCount": 0,
      "superBoostCount": 0
    }],
    getDate: () => {} 
}

describe('<ListLiveEvents />', () => {
  
  const dom = mount(<ListLiveEvents { ...props } />);

  it('does render the component', () => {
    expect(dom.length).toBe(1);
  });

  it('does render a single event', () => {
    expect(dom.find('.list-wrapper a').length).toBe(1);
    expect(dom.find('.list-wrapper a').children().length).toBe(2);
  });
});

