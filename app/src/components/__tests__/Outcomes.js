import '../../test-setup'
import React from 'react';
import { shallow, mount, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Outcomes from '../Outcomes';
Outcomes.propTypes = {}

const props = {
  "outcomes": [
    {
      "outcomeId": 367533317,
      "marketId": 93650704,
      "eventId": 21249949,
      "name": "Bandirmaspor",
      "displayOrder": 10,
      "result": {
        "place": 0,
        "result": "-",
        "favourite": false
      },
      "linkedOutcomeId": 367533405,
      "type": "home",
      "price": {
        "decimal": "2.375",
        "num": "11",
        "den": "8"
      },
      "status": {
        "active": true,
        "resulted": false,
        "cashoutable": true,
        "displayable": true,
        "suspended": true,
        "result": "-"
      }
    },
    {
      "outcomeId": 367533328,
      "marketId": 93650704,
      "eventId": 21249949,
      "name": "Draw",
      "displayOrder": 20,
      "result": {
        "place": 0,
        "result": "-",
        "favourite": false
      },
      "linkedOutcomeId": 367533414,
      "type": "draw",
      "price": {
        "decimal": "21",
        "num": "20",
        "den": "1"
      },
      "status": {
        "active": true,
        "resulted": false,
        "cashoutable": true,
        "displayable": true,
        "suspended": true,
        "result": "-"
      }
    },
    {
      "outcomeId": 367533338,
      "marketId": 93650704,
      "eventId": 21249949,
      "name": "Çorum Belediyespor",
      "displayOrder": 30,
      "result": {
        "place": 0,
        "result": "-",
        "favourite": false
      },
      "linkedOutcomeId": 367533422,
      "type": "away",
      "price": {
        "decimal": "6",
        "num": "5",
        "den": "1"
      },
      "status": {
        "active": true,
        "resulted": false,
        "cashoutable": true,
        "displayable": true,
        "suspended": true,
        "result": "-"
      }
    }
  ]
}

describe('<Outcomes />', () => {

  it('does render the component', () => {
    let dom = render(<Outcomes {...props} oddsValue='decimal' />);
    expect(dom.length).toBe(1);
    expect(dom.children().length).toBe(3)
  });

  it('does render a single event with data', () => {
    let dom = render(<Outcomes {...props} oddsValue='decimal' />);
    expect(dom.find('.outcome span:first-child').text().trim()).toBe('Win Draw Win');
  });

  it('should display display the decimal odds', () => {
    let dom = render(<Outcomes {...props} oddsValue='decimal' />);
    let outcomes = props.outcomes
    
    outcomes.forEach((outcome, i) => {
      expect(dom.find(`.outcome:nth-child(${ i + 1 }) .odds`).text()).toBe(props.outcomes[i].price.decimal);
    })
  })

  it('should display display the decimal odds', () => {
    let dom = render(<Outcomes {...props} oddsValue='fractional' />);
    let outcomes = props.outcomes
    
    outcomes.forEach((outcome, i) => {
      let num = outcomes[i].price.num
      let den = outcomes[i].price.den
      expect(dom.find(`.outcome:nth-child(${ i + 1 }) .odds`).text()).toBe(num + "/" + den);
    })
  })
});

