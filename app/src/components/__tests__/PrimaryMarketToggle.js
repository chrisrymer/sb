import '../../test-setup'
import React from 'react';
import { shallow, mount, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import PrimaryMarketToggle from '../PrimaryMarketToggle';

describe('<PrimaryMarketTogle />', () => {
  it('renders two inputs', () => {
    const inputs = mount(<PrimaryMarketToggle primaryMaket={true} />);
    expect(inputs.find('input').length).toBe(2);
  });

  it('accepts the primaryMarket prop', () => {
    const inputs = mount(<PrimaryMarketToggle primaryMaket={true} />);
    expect(inputs.props()).toEqual({"primaryMaket": true})
  });
})

