import '../../test-setup'
import React from 'react';
import { shallow, mount, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import EventControls from '../EventControls';

describe('<EventControls />', () => {
  it('does not render toggle-odds by default', () => {
    const dom = mount(<EventControls primaryMarket={false} />);
    expect(dom.find('.toggle-markets input').length).toBe(2);
    expect(dom.find('.toggle-odds').length).toBe(0);
  });

  it('does render toggle-odds if PrimaryMarket true', () => {
    const dom = mount(<EventControls primaryMarket={true} oddsValue='decimal' />);
    expect(dom.find('.toggle-odds').length).toBe(1);
  });
});

