import React from 'react';
import ListItem from './ListItem';

const styles ={
  listWrapper: {
    padding: '1em',
  }
}

const ListLiveGame = (props) => {

  return (
    <div>
        <div className="list-wrapper" style={ styles.listWrapper }>
          { 
            props.events.map((event, key) => { 
              const {eventId} = event;
              return <ListItem 
                key={eventId} 
                event={event}
                oddsValue={ props.oddsValue }
                getDate={props.getDate}
                primaryMarket={props.primaryMarket} />
            })
          }
        </div>
    </div>
  );
}

export default ListLiveGame;