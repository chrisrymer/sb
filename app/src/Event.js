import React from 'react';

const Event = (props) => {
  return (
    <div className='event'>
        <h1>Leeds V Bradford</h1>
      <div className='fixture'>
        <p className='duration'>91</p>
        <p>
          <span className='home'>LEEDS <span className='score'>3</span></span> | 
          <span className='away'>BFD <span className='score'>3</span></span>
        </p>
      </div>
    </div>
  )
}

export default Event;